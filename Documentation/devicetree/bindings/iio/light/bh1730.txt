* ROHM BH1730FVC ambient light sensor

http://www.rohm.com/web/global/products/-/product/BH1730FVC

Required properties:

  - compatible : should be "rohm,bh1730fvc"
  - reg : the I2C address of the sensor

Example:

bh1730fvc@29 {
	compatible = "rohm,bh1730fvc";
	reg = <0x29>;
};
